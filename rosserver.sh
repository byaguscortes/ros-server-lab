#!/bin/sh
sudo touch /etc/apt/sources.list.d/ros-latest.list
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu trusty main" > /etc/apt/sources.list.d/ros-latest.list'
sudo wget https://raw.githubusercontent.com/ros/rosdistro/master/ros.key
sudo apt-key add ros.key 
sudo apt-get update -y 
sudo apt-get install ros-indigo-desktop-full -y
sudo apt-get install ros-indigo-turtlesim -y
sudo sh -c 'echo "source /opt/ros/indigo/setup.bash" >> /home/vagrant/.bashrc'
sudo chown vagrant:vagrant /home/vagrant/catkin_ws/ -Rvf
sh -c 'source /opt/ros/indigo/setup.bash; nohup roscore &'